import java.util.Arrays;

public class UseArray {

    public int findMinNumOfArray(int[] nums) {
        int min = nums[0];
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] < min) {
                min = nums[i];
            }
        }
        return min;
    }

    public int findMaxNumOfArray(int[] nums) {
        int max = nums[0];
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > max) {
                max = nums[i];
            }
        }
        return max;
    }

    public int findIndexOfMin(int[] nums) {
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] < nums[j]) {
                j = i;
            }
        }
        return j;
    }

    public int findIndexOfMax(int[] nums) {
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > nums[j]) {
                j = i;
            }
        }
        return j;
    }

    public int calculateSumOfNumbersWithOddIndex(int[] nums) {
        int sum = 0;
        for (int i = 1; i < nums.length; i += 2) {
            sum += nums[i];
        }
        return sum;
    }

    public String makeReversOfArray(int[] nums) {
        int j = 0;
        int[] array = new int[nums.length];
        for (int i = nums.length - 1; i >= 0; i--) {
            array[j] = nums[i];
            j++;
        }
        return Arrays.toString(array);
    }

    public int getCountOfOddNumbersOfArray(int[] nums) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 != 0) {
                count = count + 1;
            }
        }
        return count;
    }

    public String changeHalvesOfArray(int[] nums) {
        int size = nums.length / 2;
        int i;
        int[] array = new int[nums.length];
        for (i = 0; i < size; i++) {
            array[i] = nums[size + i];
            array[size + i] = nums[i];
        }
        return Arrays.toString(array);
    }

    public String sortArrayBubble(int[] nums) {
        int size = nums.length;
        int x, i;
        for (i = 0; i < size; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                if (nums[j] > nums[j + 1]) {
                    x = nums[j + 1];
                    nums[j + 1] = nums[j];
                    nums[j] = x;
                }
            }
        }
        return Arrays.toString(nums);
    }

    public String sortArraySelect(int[] nums) {
        int i;
        for (i = 0; i < nums.length; i++) {
            int x = i;
            int min = nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[j] < min) {
                    x = j;
                    min = nums[j];
                }
            }
            nums[x] = nums[i];
            nums[i] = min;
        }
        return Arrays.toString(nums);
    }

    public String sortArrayInsert(int[] nums) {
        int i;
        for (i = 0; i < nums.length; i++) {
            int j = nums[i];
            int k = i - 1;
            for (; k >= 0; k--) {
                if (j < nums[k]) {
                    nums[k + 1] = nums[k];
                } else {
                    break;
                }
            }
            nums[k + 1] = j;
        }
        return Arrays.toString(nums);
    }
}

