import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestConditionVariables {
    ConditionVariables conditionVariables;

    @Before
    public void start() {
        conditionVariables = new ConditionVariables();
    }

    @Test
    public void testgetEvenAndOddIfAEven() {
        int expected = 24;
        int actual = conditionVariables.getEvenAndOdd(4, 6);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetEvenAndOddIfAOdd() {
        int expected = 13;
        int actual = conditionVariables.getEvenAndOdd(7, 6);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetQuarterOfCoordinatesFirstQuarter() {
        String expected = "Точка принадлежит I четверти";
        String actual = conditionVariables.getQuarterOfCoordinates(1, 1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetQuarterOfCoordinatesSecondQuarter() {
        String expected = "Точка принадлежит II четверти";
        String actual = conditionVariables.getQuarterOfCoordinates(-1, 1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetQuarterOfCoordinatesThirdQuarter() {
        String expected = "Точка принадлежит III четверти";
        String actual = conditionVariables.getQuarterOfCoordinates(-1, -1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetQuarterOfCoordinatesFourthQuarter() throws Exception {
        String expected = "Точка принадлежит IV четверти";
        String actual = conditionVariables.getQuarterOfCoordinates(1, -1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetQuarterOfCoordinatesBetweenFirstAndSecond() {
        String expected = "Точка лежит на оси между I и II четвертями";
        String actual = conditionVariables.getQuarterOfCoordinates(0, 1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetQuarterOfCoordinatesBetweenSecondAndThird() {
        String expected = "Точка лежит на оси между II и III четвертями";
        String actual = conditionVariables.getQuarterOfCoordinates(-3, 0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetQuarterOfCoordinatesBetweenThirdAndFourth() {
        String expected = "Точка лежит на оси между III и IV четвертями";
        String actual = conditionVariables.getQuarterOfCoordinates(0, -3);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetQuarterOfCoordinatesBetweenFirstAndFourth() {
        String expected = "Точка лежит на оси между I и IV четвертями";
        String actual = conditionVariables.getQuarterOfCoordinates(6, 0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetQuarterOfCoordinatesCenter() {
        String expected = "Точка в центре координатной плоскости";
        String actual = conditionVariables.getQuarterOfCoordinates(0, 0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetSumOfPositiveNumbersOnlyAPositive() {
        int expected = 2;
        int actual = conditionVariables.getSumOfPositiveNumbers(2, -5, -9);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetSumOfPositiveNumbersOnlyBPositive() {
        int expected = 5;
        int actual = conditionVariables.getSumOfPositiveNumbers(-2, 5, -9);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetSumOfPositiveNumbersOnlyCPositive() {
        int expected = 9;
        int actual = conditionVariables.getSumOfPositiveNumbers(-2, -5, 9);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetSumOfPositiveNumbersAAndBPositive() {
        int expected = 7;
        int actual = conditionVariables.getSumOfPositiveNumbers(2, 5, -9);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetSumOfPositiveNumbersOnlyABCPositive() {
        int expected = 16;
        int actual = conditionVariables.getSumOfPositiveNumbers(2, 5, 9);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetSumOfPositiveNumbersOnlyAAndCPositive() {
        int expected = 11;
        int actual = conditionVariables.getSumOfPositiveNumbers(2, -5, 9);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetSumOfPositiveNumbersOnlyBAndCPositive() {
        int expected = 14;
        int actual = conditionVariables.getSumOfPositiveNumbers(-7, 5, 9);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetMaxMultiplicationIfAllPositive() {
        int expected = 63;
        int actual = conditionVariables.getMax(2, 5, 6);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetMaxSumIfHaveZero() {
        int expected = 14;
        int actual = conditionVariables.getMax(0, 5, 6);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetMaxMultiplicationIfOneOdd() {
        int expected = 4;
        int actual = conditionVariables.getMax(2, 5, -6);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetStudentGradeF() {
        String expected = "Оценка F";
        String actual = conditionVariables.getStudentGrade(19);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetStudentGradeE() {
        String expected = "Оценка E";
        String actual = conditionVariables.getStudentGrade(30);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetStudentGradeD() {
        String expected = "Оценка D";
        String actual = conditionVariables.getStudentGrade(50);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetStudentGradeC() {
        String expected = "Оценка C";
        String actual = conditionVariables.getStudentGrade(66);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetStudentGradeB() {
        String expected = "Оценка B";
        String actual = conditionVariables.getStudentGrade(88);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetStudentGradeA() {
        String expected = "Оценка A";
        String actual = conditionVariables.getStudentGrade(99);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetStudentError() {
        String expected = "Баллы введены некорректно";
        String actual = conditionVariables.getStudentGrade(150);
        Assert.assertEquals(expected, actual);
    }

}
