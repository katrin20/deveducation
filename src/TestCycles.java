import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestCycles {
    Cycles cycles;

    @Before
    public void start() {
        cycles = new Cycles();
    }

    @Test
    public void testgetSumAndQuantity() {
        String expected = "[49, 2450]";
        String actual = cycles.getSumAndQuantity();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testcheckPrimeOrNotIfPrime() {
        String expected = "Число простое";
        String actual = cycles.checkPrimeOrNot(11);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testcheckPrimeOrNotIfNot() {
        String expected = "Число не относится к простым";
        String actual = cycles.checkPrimeOrNot(55);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testcalculateSquareRootIfZero() {
        int expected = 0;
        int actual = cycles.calculateSquareRoot(0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testcalculateSquareRoot() {
        int expected = 2;
        int actual = cycles.calculateSquareRoot(5);
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void testcalculateFactorial() throws Exception {
        int expected = 120;
        int actual = cycles.calculateFactorial(5);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testcalculateSumOfNumbers3() {
        int expected = 6;
        int actual = cycles.calculateSumOfNumbers(123);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testmakeMirror() {
        int expected = 543;
        int actual = cycles.makeMirrorOfNum(345);
        Assert.assertEquals(expected, actual);
    }
}

