public class ConditionVariables {

    public int getEvenAndOdd(int num1, int num2) {
        return (num1 % 2 == 0) ? num1 * num2 : num1 + num2;
    }

    public String getQuarterOfCoordinates(int x, int y) {
        String result;
        if (x > 0 && y > 0) {
            result = "Точка принадлежит I четверти";
        } else if (x < 0 && y > 0) {
            result = "Точка принадлежит II четверти";
        } else if (x < 0 && y < 0) {
            result = "Точка принадлежит III четверти";
        } else if (x > 0 && y < 0) {
            result = "Точка принадлежит IV четверти";
        } else if (x == 0 && y > 0) {
            result = "Точка лежит на оси между I и II четвертями";
        } else if (x < 0 && y == 0) {
            result = "Точка лежит на оси между II и III четвертями";
        } else if (x == 0 && y < 0) {
            result = "Точка лежит на оси между III и IV четвертями";
        } else if (x > 0 && y == 0) {
            result = "Точка лежит на оси между I и IV четвертями";
        } else {
            result = "Точка в центре координатной плоскости";
        }
        return result;
    }

    public int getSumOfPositiveNumbers(int a, int b, int c) {
        int sum = 0;
        if (a > 0) {
            sum += a;
        }
        if (b > 0) {
            sum += b;
        }
        if (c > 0) {
            sum += c;
        }
        return sum;
    }

    public int getMax(int a, int b, int c) {
        int result;
        int sum = a + b + c;
        int multi = a * b * c;
        if (multi > sum) {
            result = (multi + 3);
        } else {
            result = (sum + 3);
        }
        return result;
    }

    public String getStudentGrade(int rate) {
        String grade;
        if (rate <= 19) {
            grade = "Оценка F";
        } else if (rate <= 39) {
            grade = "Оценка E";
        } else if (rate <= 59) {
            grade = "Оценка D";
        } else if (rate <= 74) {
            grade = "Оценка C";
        } else if (rate <= 89) {
            grade = "Оценка B";
        } else if (rate <= 100) {
            grade = "Оценка A";
        } else {
            grade = "Баллы введены некорректно";
        }
        return grade;
    }
}
