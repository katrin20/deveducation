import java.util.Arrays;

public class Cycles {

    public String getSumAndQuantity() {
        int sum = 0;
        int count = 0;
        int[] result = new int[2];
        for (int i = 2; i < 100; i = i + 2) {
            count++;
            sum += i;
        }
        result[0] = count;
        result[1] = sum;
        return Arrays.toString(result);
    }

    public String checkPrimeOrNot(int num) {
        String prime = "Число простое";
        String notprime = "Число не относится к простым";
        for (int i = 2; i < num / 2; i++) {
            if (num % i == 0) {
                return notprime;
            }
        }
        return prime;
    }

    public int calculateSquareRoot(int num) {
        int result = 0;
        for (int i = 1; i * i <= num; i++) {
            result = i;
        }
        return result;
    }

    public int calculateFactorial(int num) throws Exception {
        int result = 1;
        if (num <= 0) {
            throw new Exception("Cannot be negative number");
        }
        if (num < 2) {
            return result;
        }
        for (int i = num; i > 0; i--) {
            result *= i;
        }
        return result;
    }

    public static int calculateSumOfNumbers (int num) {
        int sum = 0;
        int a;
        while (num > 0) {
            a = num % 10;
            num = num / 10;
            sum += a;
        }
        return sum;
    }

    public static int makeMirrorOfNum (int num) {
        int a;
        int b = 0;
        while (num > 0) {
            a = num % 10;
            num = num / 10;
            b *= 10;
            b += a;
        }
        return b;
    }
}

