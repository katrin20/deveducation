import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestUseArray {
    UseArray useArray;
    int[] nums = {4, 8, -1, 2, 0, 0, 8, 9, -10, 1};

    @Before
    public void srart() {
        useArray = new UseArray();
    }

    @Test
    public void testfindMinNumOfArray() {
        int expected = -10;
        int actual = useArray.findMinNumOfArray(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testfindMaxNumOfArray() {
        int expected = 9;
        int actual = useArray.findMaxNumOfArray(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testfindIndexOfMin() {
        int expected = 8;
        int actual = useArray.findIndexOfMin(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testfindIndexOfMax() {
        int expected = 7;
        int actual = useArray.findIndexOfMax(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testcalculateSumOfNumbersWithOddIndex() {
        int expected = 20;
        int actual = useArray.calculateSumOfNumbersWithOddIndex(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testmakeReversOfArray() {
        String expected = "[1, -10, 9, 8, 0, 0, 2, -1, 8, 4]";
        String actual = useArray.makeReversOfArray(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testcalgetCountOfOddNumbersOfArray() {
        int expected = 3;
        int actual = useArray.getCountOfOddNumbersOfArray(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testchangeHalvesOfArray() {
        String expected = "[0, 8, 9, -10, 1, 4, 8, -1, 2, 0]";
        String actual = useArray.changeHalvesOfArray(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testsortArrayBubble() {
        String expected = "[-10, -1, 0, 0, 1, 2, 4, 8, 8, 9]";
        String actual = useArray.sortArrayBubble(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testsortArraySelect() {
        String expected = "[-10, -1, 0, 0, 1, 2, 4, 8, 8, 9]";
        String actual = useArray.sortArraySelect(nums);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testsortArrayInsert() {
        String expected = "[-10, -1, 0, 0, 1, 2, 4, 8, 8, 9]";
        String actual = useArray.sortArrayInsert(nums);
        Assert.assertEquals(expected, actual);
    }
}
