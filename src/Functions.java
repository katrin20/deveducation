import static java.lang.Math.sqrt;

public class Functions {

    public String getDayOfWeek(int n) {
        String day;
        switch (n) {
            case 1:
                day = "Понедельник";
                break;
            case 2:
                day = "Вторник";
                break;
            case 3:
                day = "Среда";
                break;
            case 4:
                day = "Четверг";
                break;
            case 5:
                day = "Пятница";
                break;
            case 6:
                day = "Суббота";
                break;
            case 7:
                day = "Воскресенье";
                break;
            default:
                day = "Такого дня недели не существует!";
        }
        return day;
    }

    public String getDayOfWeekUseArray(int i) {
        String[] array = {"Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"};
        String day;
        if (i < 1 || i > 7) {
            day = "Такого дня недели не существует!";
        } else day = array[i - 1];
        return day;
    }

    public double getDistanceBetweenPoints(int x1, int y1, int x2, int y2) {
        double d = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        return d;
    }

    public String getNumberInWords(int num) {
        String result;
        String[] below_twenty = {"", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] twenty = {"", "", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] hundreds = {"", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        if (num < 0 || num > 999) {
            result = "Вы вышли за диапазон";
        } else if (num == 0) {
            result = "ноль";
        } else if (num < 20) {
            result = below_twenty[num];
        } else if (num > 19 && num < 100) {
            int a = num / 10;
            int b = num % 10;
            result = twenty[a] + " " + below_twenty[b];
        } else {
            int a = num / 100;
            int b = num % 100;
            int c = num % 10;
            if (b < 20) {
                result = hundreds[a] + " " + below_twenty[b];
            } else {
                b = b / 10;
                result = hundreds[a] + " " + twenty[b] + " " + below_twenty[c];
            }
        }
        return result;
    }

    public int getWordsInNumbers(String num) {
        String[] below_tens = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"};
        String[] tens = {"десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] twenty = {"", "", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] hundreds = {"", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        int result = 0;
        String[] words = num.split("\\s");
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < hundreds.length; j++) {
                if (words[i].equals(hundreds[j])) {
                    result += j * 100;
                } else if (words[i].equals(twenty[j])) {
                    result += j * 10;
                } else if (words[i].equals(tens[j])) {
                    result += j + 10;
                } else if (words[i].equals(below_tens[j])) {
                    result += j;
                }
            }
        }
        return result;
    }
}
