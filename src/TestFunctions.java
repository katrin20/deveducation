import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestFunctions {
    Functions functions;

    @Before
    public void start() {
        functions = new Functions();
    }

    @Test
    public void testgetDayOfWeek() {
        String expected = "Воскресенье";
        String actual = functions.getDayOfWeek(7);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetDayOfWeekUseArray() {
        String expected = "Пятница";
        String actual = functions.getDayOfWeekUseArray(5);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetDistanceBetweenPoints() {
        double expected = 4;
        double actual = functions.getDistanceBetweenPoints(5, 0, 1, 0);
        double delta = 0.00;
        Assert.assertEquals(expected, actual, delta);
    }

    @Test
    public void testgetNumberInWords() {
        String expected = "семьдесят девять";
        String actual = functions.getNumberInWords(79);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testgetWordsInNumbers() {
        int expected = 99;
        int actual = functions.getWordsInNumbers("девяносто девять");
        Assert.assertEquals(expected, actual);
    }
}
